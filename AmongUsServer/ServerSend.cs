﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace AmongUsServer
{
    class ServerSend
    {
        public static void SendTCPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Server.clients[_toClient].tcp.SendData(_packet);
        }

        private static void SendUDPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Server.clients[_toClient].udp.SendData(_packet);
        }

        private static void SendTCPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
        private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Server.clients[i].tcp.SendData(_packet);
                }
            }
        }

        private static void SendUDPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
        private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Server.clients[i].udp.SendData(_packet);
                }
            }
        }

        #region Packets
        public static void Welcome(int _toClient, string _msg)
        {
            using (Packet _packet = new Packet((int)ServerPackets.welcome))
            {
                _packet.Write(_msg);
                _packet.Write(_toClient);

                SendTCPData(_toClient, _packet);
            }
        }

        public static void StartGame()
        {
            using (Packet packet = new Packet((int)ServerPackets.startGame))
            {
                packet.Write(new Vector3(-22.16018f, 1, -7.685795f));
                packet.Write(Quaternion.Identity);

                UpdatePlayerLocationAtStart();

                SendTCPDataToAll(packet);
            }
        }

        private static void UpdatePlayerLocationAtStart()
        {
            for (int i = 1; i <= Server.clients.Count; i++)
            {
                //Once the last user logs on, a "handshake" takes place and then the player is sent into the game, the handshake process is long
                //and the player object is not yet initialized, which causes null reference exception here, skipping that player is the fix.
                if (Server.clients[i].player == null) 
                {
                    continue;
                }
                Server.clients[i].player.position = new Vector3(-22.16018f, 1, -7.685795f);
                Server.clients[i].player.rotation = Quaternion.Identity;
            }
        }

        public static void SpawnPlayer(int _toClient, Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.username);
                _packet.Write(_player.position);
                _packet.Write(_player.rotation);

                SendTCPData(_toClient, _packet);
            }
        }

        public static void PlayerPosition(int _id, Vector3 location)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
            {
                _packet.Write(_id);
                _packet.Write(location);

                SendTCPDataToAll(_packet);
            }
        }

        public static void PlayerPosition(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.position);

                SendUDPDataToAll(_packet);
            }
        }

        public static void Imposter(int id)
        {
            using (Packet packet = new Packet((int)ServerPackets.isImposter))
            {
                packet.Write(id);
                packet.Write(true);

                SendTCPData(id, packet);
            }
        }

        public static void PlayerRotation(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerRotation))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.rotation);

                SendUDPDataToAll(_player.id, _packet);
            }
        }
        #endregion
    }
}
